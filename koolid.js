//Grupp koole = [], kus sees on mitu kooli: [[],""], esimene on kooli asukoha koordinaadid, teine on kooli nimi.

var allTheSchools = [
//0
[[[59.4342, 24.7525],"Eesti Kunstiakadeemia"]],

//1
[[[58.38875, 26.69289],"Eesti Maaülikool"]],

//2
[[[59.43123, 24.74808],"Eesti Muusika- ja Teatriakadeemia"]],

//3
[[[59.3953, 24.67175],"Tallinna Tehnikaülikool"], 
[[58.25488, 22.48956],"Tallinna Tehnikaülikooli Kuressaare Kolledž"],
[[59.42937, 24.74141],"Tallinna Tehnikaülikooli Tallinna Kolledž"],
[[59.40191, 27.29028],"Tallinna Tehnikaülikooli Virumaa Kolledž"],
[[58.38927, 26.73015],"Tallinna Tehnikaülikooli Tartu Kolledž"],
[[59.46163, 24.66636],"Tallinna Tehnikaülikooli Eesti Mereakadeemia"]],

//4
[[[59.43874, 24.77167],"Tallinna Ülikool"],
[[59.34614, 26.3553],"Tallinna Ülikooli Rakvere Kolledž"],
[[58.93527, 23.54119],"Tallinna Ülikooli Haapsalu Kolledž"]],

//5
[[[58.38106, 26.71991],"Tartu Ülikool"],
[[59.37898, 28.19965],"Tartu Ülikooli Narva Kolledž"],
[[58.38523, 24.4882],"Tartu Ülikooli Pärnu Kolledž"],
[[58.36674, 25.59757],"Tartu Ülikooli Viljandi Kultuuriakadeemia"],
[[58.37909, 26.71701],"Tartu Ülikooli Euroopa Kolledž"]],

//6
[[[59.43212, 24.7566],"Estonian Business School"]],

//7
[[[58.37259, 26.72227],"Kaitseväe Ühendatud Õppeasutused"]],

//8
[[[59.30479, 26.45449],"Lääne-Viru Rakenduskõrgkool"]],

//9
[[[59.45539, 24.84288],"Sisekaitseakadeemia"]],

//10
[[[59.42644, 24.74141],"Tallinna Tehnikakõrgkool"]],

//11
[[[59.40895, 24.70937],"Tallinna Tervishoiu Kõrgkool"]],

//12
[[[58.37008, 26.73032],"Tartu Kõrgem Kunstikool"]],

//13
[[[58.31074, 26.69345],"Eesti Lennuakadeemia"]],

//14
[[[58.36518, 26.69131],"Tartu Tervishoiu Kõrgkool"]],

//15 Tallinn
[[[59.43831, 24.74674],"EELK Usuteaduste Instituut"],
[[58.38871, 26.7237],"EELK Usuteaduse Instituudi Tartu Teoloogia Akadeemia"]],

//16
[[[58.37736, 26.77611],"Eesti EKB Liit Kõrgem Usuteaduslik Seminar"]],

//17
[[[59.42272, 24.80011],"Eesti Ettevõtluskõrgkool Mainor"]],

//18
[[[59.39545, 24.66437],"Eesti Infotehnoloogia Kolledž"]],

//19
[[[59.43984, 24.77591],"Eesti Metodisti Kiriku Teoloogiline Seminar"]],

//20
[[[59.42783, 24.70215],"Euroakadeemia"]]
];