var map = L.map('map').setView([58.6, 25], 7);

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
}).addTo(map);

function style(feature) {
    return {
        weight: 1,
        opacity: 0,
        dashArray: '3',
        color:'black',
        fillOpacity: 0
    };
}

var mapStorage;

function highlightFeature(e) {
    e.target.setStyle({
        opacity: 0.7
    });
}

function resetHighlight(e) {
    mapStorage.resetStyle(e.target);
}

var layerGroup = null;

function showSchools(e){
    if(layerGroup != null){
        map.removeLayer(layerGroup);
    }
    var digit = 0;
    var markers = [];
    e.target.feature.properties.KOOLID.forEach(function(data){
        allTheSchools[data].forEach(function(school){
            var marker = L.marker(school[0], {icon : getIcon(digit)});
            marker.bindPopup(school[1]);
            markers.push(marker);
        });
        digit++;
    });
    layerGroup = L.layerGroup(markers).addTo(map);
    markers = [];
}

function getIcon(digit){
    var colour = getColor(digit);
    var icon = L.MakiMarkers.icon({icon: "marker", color: colour, size: "m"});;
    return icon;
}

function getColor(d) {
    return d > 13  ? '#CCFF00' :
           d > 12  ? '#CC3300' :
           d > 11  ? '#660000' :
           d > 10  ? '#0000FF' :
           d > 9   ? '#FF99FF' :
           d > 8   ? '#FF00FF' :
           d > 7   ? '#99FFCC' :
           d > 6   ? '#99FF00' :
           d > 5   ? '#9966FF' :
           d > 4   ? '#990033' :
           d > 3   ? '#660000' :
           d > 2   ? '#330033' :
           d > 1   ? '#C0C0C0' :
           d > 0   ? '#00CCCC' :
                     '#CC9933' ;
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: showSchools
    });
}

mapStorage = L.geoJson(countys, {style: style, onEachFeature: onEachFeature }).addTo(map);